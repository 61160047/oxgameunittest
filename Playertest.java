import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import oxoop.Player;

public class Playertest {
    
    public Playertest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    @Test
    public void testPlayer(){
        Player p1 = new Player('x');
        Player p2 = new Player('o');
        assertEquals('x',p1.getName());
    }
}
