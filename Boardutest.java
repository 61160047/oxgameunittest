import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import oxoop.Board;
import oxoop.Player;

public class Boardutest {
    
    public Boardutest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    @Test
    public void testcol1(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setRowColumn(1,1);
        board.setRowColumn(1,2);
        board.setRowColumn(1,3);
        assertEquals(true,board.checkWin());
    }
    @Test
    public void testcol2(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setRowColumn(1,3);
        board.setRowColumn(1,2);
        board.setRowColumn(1,1);
        assertEquals(true,board.checkWin());
    }
    @Test
    public void testwin1(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setRowColumn(1,1);
        board.setRowColumn(2,2);
        board.setRowColumn(3,3);
        assertEquals(true,board.checkWin());
    }
    @Test
    public void testwin2(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setRowColumn(1,1);
        board.setRowColumn(2,1);
        board.setRowColumn(3,1);
        assertEquals(true,board.checkWin());
    }
    @Test
    public void testrow1(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setRowColumn(1,1);
        board.setRowColumn(2,1);
        board.setRowColumn(3,1);
        assertEquals(true,board.checkWin());
    }
    @Test
    public void testrow2(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setRowColumn(3,1);
        board.setRowColumn(2,1);
        board.setRowColumn(1,1);
        assertEquals(true,board.checkWin());
    }
    @Test
    public void testrow3(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.setRowColumn(2,1);
        board.setRowColumn(1,1);
        board.setRowColumn(3,1);
        assertEquals(true,board.checkWin());
    }
    @Test
    public void testswitch1(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.switchPlayer();
        assertEquals('x',board.getCurrentplayer().getName());
    }
    @Test
    public void testswitch2(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.switchPlayer();
        board.switchPlayer();
        assertEquals('o',board.getCurrentplayer().getName());
    }
    
}
